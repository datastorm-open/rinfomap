#include "Infomap.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <cstdlib>
#include <string>
#include "io/Config.h"
#include "infomap/InfomapContext.h"
#include "io/HierarchicalNetwork.h"
#include "infomap/MultiplexNetwork.h"


#include <Rcpp.h>
using namespace Rcpp;

namespace infomap
{
  class InfomapR : public Infomap
  {
  public:
    InfomapR(const  std::string flags):Infomap(flags) {}
  };   

  void printClusters(infomap::HierarchicalNetwork& tree)
  {
    std::cout << "\nClusters:\n#originalIndex clusterIndex:\n";
    
    for (infomap::LeafIterator leafIt(&tree.getRootNode()); !leafIt.isEnd(); ++leafIt)
      std::cout << leafIt->originalLeafIndex << " " << leafIt->parentNode->parentIndex << '\n';
  }
  
  Rcpp::DataFrame prepareStats(const std::vector<infomap::PerIterationStats> stats) {
    std::vector<unsigned int> iterationIndex;
    std::vector<unsigned int> numTopModules;
    std::vector<unsigned int> numBottomModules;  
    std::vector<double> topPerplexity;  
    std::vector<double> bottomPerplexity;  
    std::vector<double> topOverlap;  
    std::vector<double> bottomOverlap; 
    std::vector<double> codelength;  
    std::vector<unsigned int> maxDepth;  
    std::vector<double> weightedDepth;  
    std::vector<double> seconds;  
    std::vector<bool> isMinimum; 
    
    for( unsigned int c = 0; c < stats.size(); ++c ) {
      iterationIndex.push_back(stats[c].iterationIndex);
      numTopModules.push_back(stats[c].numTopModules);
      numBottomModules.push_back(stats[c].numBottomModules);
      topPerplexity.push_back(stats[c].topPerplexity);
      bottomPerplexity.push_back(stats[c].bottomPerplexity);
      topOverlap.push_back(stats[c].topOverlap);
      bottomOverlap.push_back(stats[c].bottomOverlap);
      maxDepth.push_back(stats[c].maxDepth);
      weightedDepth.push_back(stats[c].weightedDepth);
      codelength.push_back(stats[c].codelength);
      seconds.push_back(stats[c].seconds);
      isMinimum.push_back(stats[c].isMinimum);
      
    }
    Rcpp::DataFrame df = DataFrame::create( Named("iterationIndex") = iterationIndex,         
                                            Named("numTopModules") = numTopModules,
                                            Named("numBottomModules") = numBottomModules,         
                                            Named("topPerplexity") = topPerplexity,
                                            Named("bottomPerplexity") = bottomPerplexity,         
                                            Named("topOverlap") = topOverlap,
                                            Named("bottomOverlap") = bottomOverlap,
                                            Named("codelength") = codelength,         
                                            Named("maxDepth") = maxDepth,
                                            Named("weightedDepth") = weightedDepth,         
                                            Named("seconds") = seconds,
                                            Named("isMinimum") = isMinimum
    );
    return df;
  }
  
  NumericMatrix rebuildMat(S4 mat) {      
    
    List dimnames = mat.slot("Dimnames");
    CharacterVector rows = dimnames(0);
    CharacterVector columns  = dimnames(1);
    std::vector<std::string> names = Rcpp::as<std::vector<std::string> >(rows); 
    
    std::cout << "rows.size()" <<  rows.size() << std::endl;
    std::cout << "columns.size()" <<  columns.size() << std::endl;
    
    // allocate the matrix we will return
    NumericMatrix mat_ret(rows.size(), columns.size());
    
    IntegerVector dims = mat.slot("Dim");
    IntegerVector i = mat.slot("i");
    IntegerVector p = mat.slot("p");     
    NumericVector x = mat.slot("x");
    
    unsigned int c= p[0];
    int row_idx = 0;
    for (unsigned  int j = 1; j < p.size(); j++ ) {
      while (static_cast<unsigned int>(p[j]) > c) {
        
        std::cout   << static_cast<unsigned int>(j-1)   <<  ":" 
                  <<  static_cast<unsigned int>(i[row_idx])  <<  " : " <<  x[row_idx] << std::endl;
        mat_ret( static_cast<unsigned int>(j-1), static_cast<unsigned int>(i[row_idx]))  = x[row_idx];
        row_idx = row_idx + 1;
        c = c+1;
      }
      c = p[j];
    }
    std::cout << "Links inserted : " << row_idx << std::endl;
    return mat_ret;
  }

  Rcpp::DataFrame buildNetwork(S4 mat, infomap::Config& config ) {   
    
    // std::cout.precision(17);
    
    List dimnames = mat.slot("Dimnames");
    CharacterVector rows = dimnames(0);
    CharacterVector columns  = dimnames(1);
    std::vector<std::string> names = Rcpp::as<std::vector<std::string> >(rows); 
    
    // std::vector<std::string>::iterator it;  // declare an iterator to a vector of strings
    // int counter = 0;  // counter.
    // for(it = names.begin(); it != names.end(); it++,counter++ )    {
    //   // found nth element..print and break.
    //     std::cout<< *it << std::endl;  // prints d.
    // }
    
    IntegerVector dims = mat.slot("Dim");
    IntegerVector i = mat.slot("i");
    IntegerVector p = mat.slot("p");     
    NumericVector x = mat.slot("x");
    
    infomap::Network network(config);
    
    // std::map<int, std::string> row_names;
    // std::cout << "End" <<  i.size() << std::endl;
    // for (unsigned  int j = 0; j < dims[0]; ++j ) {
    //   row_names[j] = rows[j];
    // }
    unsigned int m_numNodes = network.addNodes(names);
    std::cout << "m_numNodes" << m_numNodes << std::endl;
    NumericMatrix mat_ret(rows.size(), columns.size());
    
    
    unsigned int c=p[0];
    int row_idx = 0;
    for (unsigned  int j = 1; j < p.size(); j++ ) {
      while (static_cast<unsigned int>(p[j]) > c) {
        // std::cout << i[row_idx] << " : " << j-1 << std::endl;
        mat_ret(  static_cast<unsigned int>(i[row_idx]), static_cast<unsigned int>(j-1))  = x[row_idx];
        row_idx = row_idx + 1;
        c = c+1;
      }
      c = p[j];
    }
    
    for (unsigned  int j = 0; j < (p.size()-1); j++ ) {
      for (unsigned  int i = 0; i < (p.size()-1); i++ ) {
        // std::cout  <<  j  <<  ":" << i << ":" << mat_ret(j, i) << std::endl;
        if(mat_ret(j,i) > 0) {
          std::cout << j+1 << ":" << i+1 << ":"  <<  mat_ret(j,i)<< std::endl;
          network.addLink( j,
                           i,
                           mat_ret(j, i));
        }
      }
    }
    
    std::cout << "Links inserted : " << row_idx << std::endl;
    std::cout << "Network built : " <<  i.size() << " links" << std::endl;
    std::cout << "Network built p.size : " <<  p.size() << " links" << std::endl;
    network.finalizeAndCheckNetwork(true, p.size()-1);
    std::cout << "Network checked : " << std::endl;
    
    network.printNetworkAsPajek("toto.net");
    
    infomap::HierarchicalNetwork resultNetwork(config);
    
    std::vector<infomap::PerIterationStats> stats;
    std::cout << "Computing Clustering : " << std::endl;
    stats = infomap::run(network, resultNetwork);
    std::cout << "Computing Clustering : " << std::endl;
    
    Rcpp::DataFrame df = prepareStats(stats);
    
    return df;
  }  
  
  Rcpp::DataFrame buildNetworkWithFile (infomap::Config& config ) { 
    
    infomap::Infomap infomapWrapper(config);
    std::vector<infomap::PerIterationStats> stats;  
    stats = infomapWrapper.run();
    Rcpp::DataFrame df = prepareStats(stats);
    return df;
  }

  Rcpp::DataFrame buildNetworkWithConfigString(S4 mat, std::string config ) {   
  
  std::cout.precision(17);
  
  List dimnames = mat.slot("Dimnames");
  CharacterVector rows = dimnames(0);
  CharacterVector columns  = dimnames(1);
  std::vector<std::string> names = Rcpp::as<std::vector<std::string> >(rows); 
  
  IntegerVector dims = mat.slot("Dim");
  IntegerVector i = mat.slot("i");
  IntegerVector p = mat.slot("p");     
  NumericVector x = mat.slot("x");
  
  NumericMatrix mat_ret(rows.size(), columns.size());
  infomap::Infomap infomapWrapper(config);
  
  unsigned int c=p[0];
  int row_idx = 0;
  for (unsigned  int j = 1; j < p.size(); j++ ) {
    while (static_cast<unsigned int>(p[j]) > c) {
      std::cout << i[row_idx] << " : " << j-1 <<  ":"  << x[row_idx] << std::endl;
      mat_ret(static_cast<unsigned int>(i[row_idx]),  static_cast<unsigned int>(j-1) )  = x[row_idx];
      infomapWrapper.addLink( static_cast<unsigned int>(i[row_idx]),
                       static_cast<unsigned int>(j-1) ,
                      x[row_idx]);
      row_idx = row_idx + 1;
      c = c+1;
    }
    c = p[j];
  }
  
  // for (unsigned  int j = 0; j < (p.size()-1); j++ ) {
  //   for (unsigned  int i = 0; i < (p.size()-1); i++ ) {
  //     // std::cout  <<  j  <<  ":" << i << ":" << mat_ret(j, i) << std::endl;
  //     if(mat_ret(j,i) > 0) {
  //       // std::cout << j+1 << ":" << i+1 << ":"  <<  mat_ret(j,i)<< std::endl;
  //       infomapWrapper.addLink( j,
  //                        i,
  //                        mat_ret(j, i));
  //     }
  //   }
  // }
  std::vector<infomap::PerIterationStats> stats;  
  stats = infomapWrapper.run();
  Rcpp::DataFrame df = prepareStats(stats);
  return df;
}}


// RCPP_EXPOSED_CLASS_NODECL(infomap::InfomapR)
RCPP_EXPOSED_CLASS_NODECL(infomap::Infomap)
RCPP_EXPOSED_CLASS_NODECL(infomap::InfomapR)
RCPP_EXPOSED_CLASS_NODECL(infomap::Config)
RCPP_EXPOSED_CLASS_NODECL(infomap::Network)
  
  
  
RCPP_MODULE(ModuleInfomapR) {
    using namespace Rcpp;
    
    class_<infomap::Config>("Config")
      .constructor()
      .constructor<const infomap::Config&>()
      .method("isMemoryNetwork", &infomap::Config::isMemoryNetwork, "isMemoryNetwork")
      .method("reset", &infomap::Config::reset, "reset")
    
      .field("parsedArgs", &infomap::Config::parsedArgs)
      .field("networkFile", &infomap::Config::networkFile)
      .field("inputFormat", &infomap::Config::inputFormat)
      .field("withMemory", &infomap::Config::withMemory)
      .field("outDirectory", &infomap::Config::outDirectory)
      .field("bipartite", &infomap::Config::bipartite)
      .field("skipAdjustBipartiteFlow", &infomap::Config::skipAdjustBipartiteFlow)
      .field("multiplexAddMissingNodes", &infomap::Config::multiplexAddMissingNodes)
      .field("hardPartitions", &infomap::Config::hardPartitions)
      .field("nonBacktracking", &infomap::Config::nonBacktracking)
      .field("parseWithoutIOStreams", &infomap::Config::parseWithoutIOStreams)
      .field("zeroBasedNodeNumbers", &infomap::Config::zeroBasedNodeNumbers)
      .field("includeSelfLinks", &infomap::Config::includeSelfLinks)
      .field("ignoreEdgeWeights", &infomap::Config::ignoreEdgeWeights)
      .field("completeDanglingMemoryNodes", &infomap::Config::completeDanglingMemoryNodes)
      .field("nodeLimit", &infomap::Config::nodeLimit)
      .field("weightThreshold", &infomap::Config::weightThreshold)
      .field("preClusterMultiplex", &infomap::Config::preClusterMultiplex)
      .field("clusterDataFile", &infomap::Config::clusterDataFile)
      .field("noInfomap", &infomap::Config::noInfomap)
      .field("twoLevel", &infomap::Config::twoLevel)
      .field("directed", &infomap::Config::directed)
      .field("undirdir", &infomap::Config::undirdir)
      .field("outdirdir", &infomap::Config::outdirdir)
      .field("rawdir", &infomap::Config::rawdir)
      .field("recordedTeleportation", &infomap::Config::recordedTeleportation)
      .field("teleportToNodes", &infomap::Config::teleportToNodes)
      .field("teleportationProbability", &infomap::Config::teleportationProbability)
      .field("selfTeleportationProbability", &infomap::Config::selfTeleportationProbability)
      .field("markovTime", &infomap::Config::markovTime)
      .field("variableMarkovTime", &infomap::Config::variableMarkovTime)
      .field("preferredNumberOfModules", &infomap::Config::preferredNumberOfModules)
      .field("multiplexRelaxRate", &infomap::Config::multiplexRelaxRate)
      .field("multiplexJSRelaxRate", &infomap::Config::multiplexJSRelaxRate)
      .field("multiplexJSRelaxLimit", &infomap::Config::multiplexJSRelaxLimit)
      .field("multiplexRelaxLimit", &infomap::Config::multiplexRelaxLimit)
      .field("seedToRandomNumberGenerator", &infomap::Config::seedToRandomNumberGenerator)
      .field("numTrials", &infomap::Config::numTrials)
      .field("minimumCodelengthImprovement", &infomap::Config::minimumCodelengthImprovement)
      .field("minimumSingleNodeCodelengthImprovement", &infomap::Config::minimumSingleNodeCodelengthImprovement)
      .field("randomizeCoreLoopLimit", &infomap::Config::randomizeCoreLoopLimit)
      .field("coreLoopLimit", &infomap::Config::coreLoopLimit)
      .field("levelAggregationLimit", &infomap::Config::levelAggregationLimit)
      .field("tuneIterationLimit", &infomap::Config::tuneIterationLimit)
      .field("minimumRelativeTuneIterationImprovement", &infomap::Config::minimumRelativeTuneIterationImprovement)
      .field("fastCoarseTunePartition", &infomap::Config::fastCoarseTunePartition)
      .field("alternateCoarseTuneLevel", &infomap::Config::alternateCoarseTuneLevel)
      .field("coarseTuneLevel", &infomap::Config::coarseTuneLevel)
      .field("fastHierarchicalSolution", &infomap::Config::fastHierarchicalSolution)
      .field("fastFirstIteration", &infomap::Config::fastFirstIteration)
      .field("lowMemoryPriority", &infomap::Config::lowMemoryPriority)
      .field("innerParallelization", &infomap::Config::innerParallelization)
      .field("resetConfigBeforeRecursion", &infomap::Config::resetConfigBeforeRecursion)
      .field("outName", &infomap::Config::outName)
      .field("originallyUndirected", &infomap::Config::originallyUndirected)
      .field("printTree", &infomap::Config::printTree)
      .field("printFlowTree", &infomap::Config::printFlowTree)
      .field("printMap", &infomap::Config::printMap)
      .field("printClu", &infomap::Config::printClu)
      .field("printNodeRanks", &infomap::Config::printNodeRanks)
      .field("printFlowNetwork", &infomap::Config::printFlowNetwork)
      .field("printPajekNetwork", &infomap::Config::printPajekNetwork)
      .field("printStateNetwork", &infomap::Config::printStateNetwork)
      .field("printBinaryTree", &infomap::Config::printBinaryTree)
      .field("printBinaryFlowTree", &infomap::Config::printBinaryFlowTree)
      .field("printExpanded", &infomap::Config::printExpanded)
      .field("noFileOutput", &infomap::Config::noFileOutput)
      .field("verbosity", &infomap::Config::verbosity)
      .field("verboseNumber)Precision", &infomap::Config::verboseNumberPrecision)
      .field("silent", &infomap::Config::silent)
      .field("benchmark", &infomap::Config::benchmark)
      .field("maxNodeIndexVisible", &infomap::Config::maxNodeIndexVisible)
      .field("showBiNodes", &infomap::Config::showBiNodes)
      .field("minBipartiteNodeIndex", &infomap::Config::minBipartiteNodeIndex)
      .field("version", &infomap::Config::version)
    ;
    
    class_<infomap::Network>("Network")
      // .constructor<std::string>()
         .constructor<const infomap::Config&>()
         .method("outDegree", &infomap::Network::outDegree, "return m_outDegree")
    ;
    
    class_<infomap::Infomap>("Infomap")
      // .constructor<std::string>()
      .constructor<const infomap::Config&>()
      // .method("runWithConfig", &infomap::Infomap::runWithConfig, "run the MapEquation")
      .method("addLink", &infomap::Infomap::addLink, "Add the MapEquation")
    
      .field("config", &infomap::Infomap::config)
    ;
    
    function("buildNetwork", &infomap::buildNetwork,"run the MapEquation");
    function("buildNetworkWithConfigString", &infomap::buildNetworkWithConfigString,"run the MapEquation with string");
    function("buildNetworkWithFile", &infomap::buildNetworkWithFile,"run the MapEquation from a pajek file");
    function("rebuildMat", &infomap::rebuildMat,"build Matrix");
    
}  
  
  