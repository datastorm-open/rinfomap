# Rinfomap

Le package est un wrapper sur la librairie Infomap.


## Pré requis

Pour le moment le package n'a pas été prévu Windows.

Infomap nécessite gcc ou clang

* Ubuntu
    - build-essential linux-headers-generic 
  
* Fedora
    - dnf : development-tools linux-develge

## Adaptation d'infomap   

La librairie a été adaptée pour :

### Sorties de résultats intermédiaires

### Changement de signature pour récupération des sorties
    
    
## Usage

Infomap : http://www.mapequation.org/code.htm

Infomap fournit ces résultats en mode fichier : on peut lui préciser le mode de sortie. Nous utilisons les sorties .treee et .ftree

Le package rinomap contient 2 classes "Rcpp config"

* Config qui va permettre de passer les options souhaitées à la libraiire Infomap

* Infomap qui permet d'appeller la librairie Infomap


2 possibilités :

### Résultats fichiers

```
library(rinfomap)

config=new(Class = Config)
config$networkFile = "SIMIL-ieee96.net"
config$outdirdir=  T
config$outDirectory = "output/"
config$outName = "SIMIL-ieee96.net"
config$printBinaryTree = F
config$printTree = T
config$printFlowTree = T
config$printClu = T
config$numTrials = 10
config$innerParallelization = T
config$seedToRandomNumberGenerator = 123

infomapWrapper = new (Infomap ,config)
system.time(infomapWrapper$runWithConfig())

infomapWrapper$finalize()
rm(infomapWrapper)
rm(config)
```



###  Résultats fichiers et R

``` r
library(rinfomap)
config=new(Class = Config)
config$outdirdir=  T
config$outDirectory = "output/"
config$outName = "res.net"
config$printBinaryTree = F
config$printTree = T
config$printClu = T
config$numTrials = 10
config$innerParallelization = T
config$seedToRandomNumberGenerator = 123

# load ASFranceComp matrix first
load("ASFranceComp.rda")   #  C'est une analyse de sécurité à fournir (cf package ClusterRSO)
df = buildNetwork(ASFranceComp, config)
```

``` r
str(df)
'data.frame':	10 obs. of  12 variables:
 $ iterationIndex  : num  5 9 0 7 3 8 1 6 4 2
 $ numTopModules   : num  1548 1982 1566 1978 1582 ...
 $ numBottomModules: num  2916 2933 3038 2967 2977 ...
 $ topPerplexity   : num  1.02 37.31 3.65 37.02 3.68 ...
 $ bottomPerplexity: num  538 444 1216 657 948 ...
 $ topOverlap      : num  1 1 1 1 1 1 1 1 1 1
 $ bottomOverlap   : num  1 1 1 1 1 1 1 1 1 1
 $ codelength      : num  9.44 9.47 9.48 9.49 9.49 ...
 $ maxDepth        : num  7 4 5 4 6 5 5 5 4 5
 $ weightedDepth   : num  4.21 2.46 3.31 2.55 3.26 ...
 $ seconds         : num  12.74 8.47 8.6 7.52 8.24 ...
 $ isMinimum       : logi  TRUE FALSE TRUE FALSE FALSE FALSE ...
```

* iterationIndex  : iteration Index
* numTopModules   : 
* numBottomModules: 
* topPerplexity   :  perplexity P of the module size distribution measures the effective number of modules
* bottomPerplexity:  Perplexity of bottom module flow distribution
* topOverlap      : 
* bottomOverlap   : 
* codelength      :  Information measure for clustering : target si to minimize this value
* maxDepth        :  Cluster depth
* weightedDepth   :  
* seconds         :  cpu time for iteration
* isMinimum       :  Boolean True when minimum codelength



